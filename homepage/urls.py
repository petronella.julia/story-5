from django.urls import path, include
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('behind/', views.behind, name='behind'),
    path('schedule', include('schedule.urls')),
]
